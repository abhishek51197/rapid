echo 'Starting Basic Configuration'
sudo apt install python3-pip
sudo apt-get install git
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo pip3 install PyMongo
sudo start service mongod start
echo 'Enabling Security Features'
sudo ufw allow http
#sudo ufw allow 80
sudo ufw allow 22
sudo ufw allow ssh
sudo ufw allow 27017
sudo ufw enable
#Security Rules Custom
sudo chmod 755 /etc/sysctl.conf
sudo echo 'net.ipv4.conf.all.accept_redirects = 0' >> /etc/sysctl.conf
sudo echo 'net.ipv6.conf.all.accept_redirects = 0' >> /etc/sysctl.conf
sudo echo 'net.ipv4.conf.default.accept_redirects = 0 ' >> /etc/sysctl.conf
sudo echo 'net.ipv4.icmp_echo_ignore_broadcasts = 1' >> /etc/sysctl.conf
sudo echo 'net.ipv6.conf.default.accept_redirects = 0' >> /etc/sysctl.conf
sudo echo 'net.ipv4.conf.all.send_redirects = 0' >> /etc/sysctl.conf
sudo echo 'net.ipv4.conf.all.accept_source_route = 0' >> /etc/sysctl.conf
sudo echo 'net.ipv6.conf.all.accept_source_route = 0' >> /etc/sysctl.conf
sudo echo 'net.ipv4.conf.all.log_martians = 1' >> /etc/sysctl.conf
sudo echo 'net.ipv4.icmp_ignore_bogus_error_responses = 1' >> /etc/sysctl.conf
sudo echo 'net.ipv4.icmp_echo_ignore_all = 1' >> /etc/sysctl.conf
sudo sysctl -p
sudo chmod 755 /etc/host.conf
sudo echo 'order bind,host' >> /etc/host.conf
sudo echo 'nospoof on' >> /etc/host.conf
sudo mongo rapid --eval 'db.createCollection("rawdata")'
sudo mongo rapid --eval 'db.createUser( { user: "rapid",
                 pwd: "Rmx@2018",
                 roles: [ "readWrite"] } )'
sudo chmod 755 /etc/mongod.conf
sudo echo 'security:' >> /etc/mongod.conf
sudo echo '    authorization: enabled' >> /etc/mongod.conf
sudo service mongod stop
sudo service mongod start --conf /etc/mongod.conf
sudo apt-get install apache2
sudo apt-get install libapache2-mod-wsgi-py3
sudo apt-get upgrade
sudo apt-get update
sudo reboot
