// Side Bar Section
		function openNav() {
			document.getElementById("mySidenav").style.width = "15%";
			document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.body.style.backgroundColor = "#7F7F7F";
		}

		

		// NavTabs-Section
		function openPage(pageName, elmnt, color) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablink");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].style.backgroundColor = "";
			}
			document.getElementById(pageName).style.display = "block";
			elmnt.style.backgroundColor = color;
			elmnt.style.color = "#000";

		}
		// Get the element with id="defaultOpen" and click on it
		// document.getElementById("defaultOpen").click();

// Toggle the search box


function toggleSearchBox() {
	var x = document.getElementById("search_box");
	var searchIcon = document.getElementById("search_icon");
	var close = document.getElementById("search_close");
	if (x.style.display === "none") {
		x.style.display = "block";
		searchIcon.style.display = "none";
		close.style.display = "block";
	} else {
		x.style.display = "none";
		searchIcon.style.display = "block";
		close.style.display = "none";
	}
}

function closeSearchBox() {
	var x = document.getElementById("search_box");
	var searchIcon = document.getElementById("search_icon");
	var close = document.getElementById("search_close");
	if (x.style.display === "block") {
		x.style.display= "none";
		searchIcon.style.display = "block";
		close.style.display = "none";
	}
}


// Initialising tooltip
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

// date function
// $(document).ready(function() {
// 	$('input[type=date').change(function() {
// 		console.log(this.value);
// 		var inputDate = new Date(this.value);
// 		console.log(inputDate);
// 		console.log(inputDate.day)
// 	})
// });

// function increaseDate() {
// 	var date = $("#date");
// 	console.log(date);
// }



