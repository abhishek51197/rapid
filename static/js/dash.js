// angular.module('rapidMatrix', [])
var app = angular.module('rapidMatrix', ['ngMaterial', 'ngMessages' , 'infinite-scroll'])
app.directive('export',function($rootScope){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var el = element[0];
            element.bind('click', function(e){
                let keyOrder=[]
                var csvString = '';
                console.log($rootScope.products["yesProducts"])
                let Keys = {'Department':'n/a','Category':'n/a','SubCategory':'n/a','Brand':'n/a','Cost':'n/a','PriceChange':'n/a','ProfitOld':'n/a','ProfitNew':'n/a','RevenueOld':'n/a','RevenueNew':'n/a','VolumeOld':'n/a','VolumeNew':'n/a'}

                for(user in $rootScope.products["yesProducts"][0]){
                        csvString+=user+","
                        keyOrder.push(user)
                    }
                    csvString = csvString.substring(0, csvString.length - 1);
                    csvString = csvString + "\n";
                    
                for (let x=0;x<$rootScope.products['yesProducts'].length ; x++){
                    for(let y=0;y<keyOrder.length ; y++){
                    	delete $rootScope.products['yesProducts'][x]['$$hashKey'];
                        csvString+=$rootScope.products['yesProducts'][x][keyOrder[y]]+","
                    }
                    csvString = csvString.substring(0, csvString.length - 1);
                    csvString = csvString + "\n";
                }
                csvString = csvString.substring(0, csvString.length - 1);
                var a = $('<a/>', {
                    style:'display:none',
                    href:'data:application/octet-stream;base64,'+btoa(csvString),
                    download:'yesProductsreports.csv'
                }).appendTo('body')
                a[0].click()
                a.remove();
            });
        }
    }
    })


app.directive('export1',function($rootScope){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var el = element[0];
            element.bind('click', function(e){
                let keyOrder=[]
                var csvString = '';
                console.log($rootScope.products["testProducts"])
                let Keys = {'Department':'n/a','Category':'n/a','SubCategory':'n/a','Brand':'n/a','Cost':'n/a','PriceChange':'n/a','ProfitOld':'n/a','ProfitNew':'n/a','RevenueOld':'n/a','RevenueNew':'n/a','VolumeOld':'n/a','VolumeNew':'n/a'}

                for(user in $rootScope.products["testProducts"][0]){
                        csvString+=user+","
                        keyOrder.push(user)
                    }
                    csvString = csvString.substring(0, csvString.length - 1);
                    csvString = csvString + "\n";
                    
                for (let x=0;x<$rootScope.products['testProducts'].length ; x++){
                    for(let y=0;y<keyOrder.length ; y++){
                        csvString+=$rootScope.products['testProducts'][x][keyOrder[y]]+","
                    }
                    csvString = csvString.substring(0, csvString.length - 1);
                    csvString = csvString + "\n";
                }
                csvString = csvString.substring(0, csvString.length - 1);
                var a = $('<a/>', {
                    style:'display:none',
                    href:'data:application/octet-stream;base64,'+btoa(csvString),
                    download:'testReports.csv'
                }).appendTo('body')
                a[0].click()
                a.remove();
            });
        }
    }
    });



app.controller('mainCtrl', function ($scope, $http,$rootScope) {
$scope.globalSearch=""
	$scope.date = new Date();
	$scope.alertText = '';
	$scope.grid = "large";
    $rootScope.products={}
    $rootScope.products['yesProducts']=[];
     $rootScope.products['testProducts']=[]
	$scope.collapsedTable = true;
	$scope.expandedTable = false;

	$scope.value = "$";
	$scope.routeMain = 'tabs'

	// $scope.changeColor = function(id) {

	// }


	$scope.changeRoute = function (route) {
		$scope.routeMain = route;
		$scope.singleResult=false;
	}
$scope.singleResult=false;
$scope.showProduct={}
	$scope.getYesResults = function (route, i) {
		$scope.routeMain = route;
		$scope.singleResult=true;
		$scope.showProduct =$scope.products['new'][i]
	}

	$scope.onChangeDate = function () {

		console.log($scope.date);
	}
	$scope.changeDate = function (value) {
		if (value == 0) {
			let date = ($scope.date.getTime()) - 86400000;
			$scope.date = new Date(date);
		}
		else {
			date = $scope.date.getTime() + 86400000;
			currentDate = new Date().getTime()
			if (date > currentDate) { return } else { $scope.date = new Date(date) }
		}
	}


	$scope.showTopAlert = function (type, index, listName) {
		if (type == 'donate') { $scope.alertText = 'Donation Agent Notified for SKU-' + $scope.products[listName][index]['SKU'] }
		else { $scope.alertText = 'Store Manager Notified for SKU-' + $scope.products[listName][index]['SKU'] }
		$('#topAlertSuccess').fadeIn();
		setTimeout(function () {
			$('#topAlertSuccess').fadeOut();
		}, 1500);
		$scope.products[listName][index]['Action'] = type

		$scope.sendToAlert(listName, index)
	}

	$scope.changePaginations = function (keys) {
		
	}

	$scope.changeGridLayout = function (layout) {
		$scope.grid = layout;
		$scope.skip={}
		if ($scope.grid == 'small') {
			for (keys in $scope.products) {
				$scope.skip[keys] = $scope.limitSmall;
				
			}
		}
		else {
			for (keys in $scope.products) {
				$scope.skip[keys] = $scope.limit;
				
			}
		}
	}


	$scope.display = 'dollar'
	$scope.changedisplayto = function (display) {
		$scope.display = display;
	}


	// Switching 

	$scope.sendToYes = function (oldList, index) {
		$(".collapse").collapse("hide")
		$('#nav-yes-tab').addClass("highlight")
		setTimeout(function () {
			$('#nav-yes-tab').removeClass("highlight")
		}, 1000);
		$scope.products["yesProducts"].push($scope.products[oldList][index]);
		
		$rootScope.products["yesProducts"].push($scope.products[oldList][index]);
		$scope.products[oldList].splice(index, 1)
		$scope.changePaginations(oldList);
		

		$scope.changePaginations('yesProducts')
	}

	$scope.sendToNo = function (oldList, index) {
		$(".collapse").collapse("hide")
		$('#nav-no-tab').addClass("highlight")
		setTimeout(function () {
			$('#nav-no-tab').removeClass("highlight")
		}, 1000);

		// After Callback
		$scope.products["noProducts"].push($scope.products[oldList][index])
		$scope.products[oldList].splice(index, 1)
		$scope.changePaginations(oldList)
		$scope.changePaginations('noProducts')

	}



	$scope.sendToTest = function (oldList, index) {
		$(".collapse").collapse("hide")
		$('#nav-test-tab').addClass("highlight")
		setTimeout(function () {
			$('#nav-test-tab').removeClass("highlight")
		}, 1000);

		$scope.products["testProducts"].push($scope.products[oldList][index]);
        $rootScope.products["testProducts"].push($scope.products[oldList][index]);
		$scope.products[oldList].splice(index, 1)
		$scope.changePaginations(oldList)

		$scope.changePaginations('testProducts')
	}


	$scope.sendToAlert = function (oldList, index) {
		$(".collapse").collapse("hide")
		$('#nav-alert-tab').addClass("highlight")
		setTimeout(function () {
			$('#nav-alert-tab').removeClass("highlight")
		}, 1000);
		$scope.products["alertProducts"].push($scope.products[oldList][index])
		$scope.products[oldList].splice(index, 1)
		$scope.changePaginations(oldList)
		$scope.changePaginations('alertProducts')

	}

	$scope.sendToAuto = function (oldList, index) {
		$(".collapse").collapse("hide")
		$('#nav-auto-tab').addClass("highlight")
		setTimeout(function () {
			$('#nav-auto-tab').removeClass("highlight")
		}, 1000);
		$scope.products["autoProducts"].push($scope.products[oldList][index])
		$scope.products[oldList].splice(index, 1)
		$scope.changePaginations(oldList)

		$scope.changePaginations('autoProducts')
	}
	$scope.sendToNew = function (oldList, index) {
		$(".collapse").collapse("hide")
		$('#nav-new-tab').addClass("highlight")
		setTimeout(function () {
			$('#nav-new-tab').removeClass("highlight")
		}, 1000);

		$scope.products["new"].push($scope.products[oldList][index])
		$scope.products[oldList].splice(index, 1)
		$scope.changePaginations(oldList)

		$scope.changePaginations('new')
	}

	$scope.products = {}
	$scope.products["yesProducts"] = [];
	$scope.products["noProducts"] = [];
	$scope.products["testProducts"] = [];
	$scope.products["autoProducts"] = [];
	$scope.products["new"] = [];
	$scope.products["alertProducts"] = [];





	$scope.skips={}
	$scope.getData = function () {
		$http({
			method: 'POST',
			url: '/getData',
			data: {},
			headers: { 'Content-Type': 'application/json' }
		}).then(function (res) {
			$scope.products["new"]=res.data.data;
			
			console.log($scope.products["new"])
			$scope.createPaginationNew('new', Math.ceil($scope.products["new"].length / $scope.limit))
		});
	}

	$scope.getData()


	// Pagination

	$scope.limit = 4;
	$scope.limitSmall = 10;
	$scope.skip = {}
	$scope.pages = {}
	$scope.skip['new'] = 0
	$scope.pages['new'] = [];
	$scope.skip['yesProducts']=0;
	

	$scope.createPaginationNew = function (tab, number) {

		$scope.pages[tab] = []
		for (i = 0; i < number; i++) { $scope.pages[tab].push(i) }
	}





// PAgination


$scope.getProducts=function(cate){
	var limit=0;

if($scope.grid=='large'){
limit=$scope.limit
}
else{
	limit=$scope.limitSmall;
}
console.log(cate,limit)
	if(cate in $scope.skip){

		$scope.skip[cate]+=limit;

	
	}
	else{
			$scope.skip[cate]=limit;		
	}

	
	
	}


	// $scope.getData( $scope.skips[cate]+4, cate)



	// Table Code
	$scope.showExpanded = function () {
		$scope.expandedTable = true;
		$scope.collapsedTable = false;
	}

	$scope.showCollapsed = function () {
		$scope.collapsedTable = true;
		$scope.expandedTable = false;
	}
	


	// {{'{{labels.tabs.tabNew}}'}}
	// {{'{{labels.productCatagories.sku}}'}}
	// {{'{{labels.headings.headingProduct}}'}}
	// {{'{{labels.subHeads.subHeadNew}}'}}
	// {{'{{labels.buttons.Yes}}'}}



	// {{'{{labels.productCatagories.sku}}'}}
	// {{'{{labels.AccordionText.accLife}}'}}
	// {{'{{labels.AccordionTwoText.life}}'}}

$scope.labels={
	"noResult": {
		"noResultFound": "No Result Found!",
		"factorsInflencing":"Factors influencing price recommendation"
	},
    "tabs": 
        {
            "new": "New",
            "yes": "Yes",
            "no": "No",
            "tests": "Tests",
            "alert": "Alert",
            "auto": "Auto"
        }
    ,
    "productCatagories": 
        {
            "sku": "SKU",
            "department": "Department",
            "catagory": "Catagory",
            "subCatagory": "Sub-Catagory",
            "brand": "Brand",
            "cost": "Cost"
        }
    ,
    "headings": 
        {
            "product": "Product",
            "priceChange": "Price Change",
            "gmChange": "GM Change",
            "stock": "Stock"
        }
    ,
    "subHeads": 
        {
            "newVsCurrent": "New vs Current",
            "oneDay": "1 day (24 hours)",
            "inApplicable": "In applicabe units"
        }
    ,
    "sections": 
        {
            "changePrice": "Change Price",
            "currentPerUnit": "Current per unit",
            "newPerUnit": "New per Unit",
            "factors": "Factors",
            "stores": "Stock"
        }
    ,
    "buttons": 
        {
            "yes": "Yes",
            "no": "No",
            "test": "Test",
            "donate": "Donate",
            "alertManager": "Alert Manager",
            "autoPrice": "Auto Price",
            "cancel": "Cancel",
            "alerted": "Alerted*",
			"donated": "Donated*",
        }
    ,
    "text": {
        "sku": "SKU Price Recommendation Automated",
        "priceRecom": "Price recommendations will be implemented automatically in the future"
	},
	"AccordionTwoText": {
		"life": "Life Cycle Stage",
		"trend": "Trend",
		"inventory": "Inventory Level",
		"day": "Day of Week",
		"seasonality": "Seasonality",
		"grossMarginAt": "Gross Margin at",
		"suggestedPrice": "Suggested Price",
		"currentPrice": "Current Price",
		"gmChange": "GM Change"
	},
	"AccordionThreeText": {
		"grade": "Grade",
		"day": "Day",
		"qty": "Qty",
		"storeDetails": "Store Details",
		"top": "Top 5 of 15",
		"storeAddress": "Store Address",
		"stock": "Stock"
	},
	"recom": {
		"clickForCollapsed" : "Click for Collapsed View",
		"clickForExpanded" : "Click for Collapsed View",
		"exportAsCsv" : "Export as .csv",
		"priceRecommendations": "Price Recommendations",
		"scenarioId": "Scenario ID",
		"description": "Description",
		"sku": "SKU",
		"department": "Department",
		"category": "Category",
		"subCategory": "Sub Category",
		"brand": "Brand",
		"cost": "Cost",
		"priceChange": "Price Change (%)",
		"profit": "Profit ($)",
		"revenue": "Revenue ($)",
		"volume": "Volume (Units)",
		"oldPrice": "Old Price",
		"newPrice": "New Price",
		"change": "Change (%)"
	}
	,
	"result": {
		"clickForCollapsed" : "Click for Collapsed View",
		"clickForExpanded" : "Click for Collapsed View",
		"exportAsCsv" : "Export as .csv",
		"testResults": "Test Results",
		"test": "Test",
		"status": "Status",
		"description": "Description",
		"sku": "SKU",
		"department": "Department",
		"category": "Category",
		"subCategory": "Sub Category",
		"brand": "Brand",
		"cost": "Cost",
		"priceChange": "Price Change (%)",
		"profit": "Profit ($)",
		"revenue": "Revenue ($)",
		"volume": "Volume (Units)",
		"testPrice": "Test Price",
		"oldPrice": "Old Price",
		"newPrice": "New Price",
		"change": "Change (%)",
		"control": "Control"

	}
}


});


