app.controller('commonCtrl', function ($scope, $http) {

	$scope.sortFun = "SKU"
	$scope.sortCheckList = [true, true, true]

	// slider code
	$scope.minsliderValues = {}
	$scope.maxsliderValues = {}
	$scope.oldPrices = {}


	$scope.minSlider = function (index, lineProducts) {
		if ($scope.products[lineProducts]) {
			var sku = $scope.products[lineProducts][index]['SKU']

			if (sku in $scope.minsliderValues) {
				return $scope.minsliderValues[sku]
			}
			else {
				value = ($scope.products[lineProducts][index].NewPriceSlider - $scope.products[lineProducts][index].NewPriceSlider / 4);
				value = parseInt(value)
				$scope.minsliderValues[sku] = value;
				return value;
			}
		}
		else {
			return;
		}



	}
	$scope.maxSlider = function (index, lineProducts) {
		if ($scope.products[lineProducts]) {
			var sku = $scope.products[lineProducts][index]['SKU']

			if (sku in $scope.maxsliderValues) {
				return $scope.maxsliderValues[sku]
			}
			else {
				value = ($scope.products[lineProducts][index].NewPriceSlider + $scope.products[lineProducts][index].NewPriceSlider / 4);

				value = parseInt(value)
				$scope.maxsliderValues[sku] = value;
				return value;
			}
		}


	}

	$scope.allowed = [5, 9];
	$scope.sliderMoved = function (index, lineProducts) {
		let holdPrice = $scope.products[lineProducts][index].NewPriceSlider
		holdPrice = holdPrice % 10;

		var closest = $scope.allowed.reduce(function (prev, curr) {
			return (Math.abs(curr - holdPrice) < Math.abs(prev - holdPrice) ? curr : prev);
		});

		$scope.products[lineProducts][index].NewPriceSlider = parseInt($scope.products[lineProducts][index].NewPriceSlider / 10);

		$scope.products[lineProducts][index].NewPriceSlider = ($scope.products[lineProducts][index].NewPriceSlider * 10) + (closest)

		let oldprice = 0
		if ($scope.products[lineProducts][index]['SKU'] in $scope.oldPrices) {
			oldprice = $scope.oldPrices[$scope.products[lineProducts][index]['SKU']];
		}
		else {
			oldprice = $scope.products[lineProducts][index].NewPrice;
			$scope.oldPrices[$scope.products[lineProducts][index]['SKU']] = oldprice
		}

		$scope.products[lineProducts][index].NewPrice = (($scope.products[lineProducts][index].NewPriceSlider) / 100)
		v2 = ((-1.5 * $scope.products[lineProducts][index].AverageQtyDay) * ($scope.products[lineProducts][index].NewPrice - oldprice)) / oldprice + $scope.products[lineProducts][index].AverageQtyDay
		$scope.products[lineProducts][index]['ForecastedSalesDay'] = ($scope.products[lineProducts][index].NewPrice - $scope.products[lineProducts][index]['CurrentPrice']) * v2
		$scope.products[lineProducts][index]['ForecastedSalesDay'] = parseInt($scope.products[lineProducts][index]['ForecastedSalesDay'])
		$scope.products[lineProducts][index]['PricePercentage'] = $scope.getPercentage($scope.products[lineProducts][index].NewPrice, $scope.products[lineProducts][index]['CurrentPrice'])
		$scope.products[lineProducts][index]['GrossPercentage'] = $scope.getGrossPercentage(index, lineProducts)
	}
	// slider code Ends

	$scope.changeValuePrice = function (value, index, lineProducts) {
		let oldprice = 0
		if ($scope.products[lineProducts][index]['SKU'] in $scope.oldPrices) {
			oldprice = $scope.oldPrices[$scope.products[lineProducts][index]['SKU']];
		}
		else {
			oldprice = $scope.products[lineProducts][index].NewPrice;
			$scope.oldPrices[$scope.products[lineProducts][index]['SKU']] = oldprice
		}

		var tempPrice = $scope.products[lineProducts][index].NewPriceSlider
		if (value == 'plus') {
			tempPrice = tempPrice + 1
		}
		else {
			tempPrice = tempPrice - 1
		}
		if (tempPrice < $scope.minsliderValues[$scope.products[lineProducts][index]['SKU']] || tempPrice > $scope.maxsliderValues[$scope.products[lineProducts][index]['SKU']]) {
			return;
		}
		else {
			$scope.products[lineProducts][index].NewPriceSlider = tempPrice;
		}

		$scope.products[lineProducts][index].NewPrice = (($scope.products[lineProducts][index].NewPriceSlider) / 100)
		v2 = ((-1.5 * $scope.products[lineProducts][index].AverageQtyDay) * ($scope.products[lineProducts][index].NewPrice - oldprice)) / oldprice + $scope.products[lineProducts][index].AverageQtyDay
		$scope.products[lineProducts][index]['ForecastedSalesDay'] = ($scope.products[lineProducts][index].NewPrice - $scope.products[lineProducts][index]['CurrentPrice']) * v2
		$scope.products[lineProducts][index]['ForecastedSalesDay'] = parseInt($scope.products[lineProducts][index]['ForecastedSalesDay'])
		$scope.products[lineProducts][index]['PricePercentage'] = $scope.getPercentage($scope.products[lineProducts][index].NewPrice, $scope.products[lineProducts][index]['CurrentPrice'])
		$scope.products[lineProducts][index]['GrossPercentage'] = $scope.getGrossPercentage(index, lineProducts)
	}

	$scope.getPercentage = function (newPrice, currentPrice) {
		var percentage = parseInt((currentPrice - (currentPrice - newPrice)) / currentPrice * 100);
		return percentage;
	}


	$scope.getGrossPercentage = function (index, lineProducts) {
		var product = $scope.products[lineProducts][index]
		let oldprice = 0
		if ($scope.products[lineProducts][index]['SKU'] in $scope.oldPrices) {
			oldprice = $scope.oldPrices[$scope.products[lineProducts][index]['SKU']];
		}
		else {
			oldprice = $scope.products[lineProducts][index].NewPrice;
			$scope.oldPrices[$scope.products[lineProducts][index]['SKU']] = oldprice
		}
		oldGM = (oldprice - product.CurrentPrice) * product.AverageQtyDay
		v2 = ((-1.5 * $scope.products[lineProducts][index].AverageQtyDay) * ($scope.products[lineProducts][index].NewPrice - oldprice)) / oldprice + $scope.products[lineProducts][index].AverageQtyDay
		newGM = (product.NewPrice - product.CurrentPrice) * v2
		return parseInt(((newGM - oldGM) / oldGM) * 100)

	}



	$scope.changePage = function (page, lineProducts) {

		if ($scope.grid == 'large') {
			$scope.skip[lineProducts] = page * $scope.limit
		}
		else {
			$scope.skip[lineProducts] = page * $scope.limitSmall
		}


	}
	$scope.changeSort = function (key, index, lineProducts) {
		if ($scope.sortCheckList[index]) {
			$scope.products[lineProducts] = $scope.products[lineProducts].sort(function (a, b) {
				return a[key] - b[key];
			})
			console.log($scope.products[lineProducts])

		}
		else {
			$scope.products[lineProducts] = $scope.products[lineProducts].sort(function (a, b) {
				return b[key] - a[key];
			})
		}
		$scope.sortCheckList[index] = !$scope.sortCheckList[index]
	}

	$scope.drawPieChart = function (index, lineProducts, id, collapsibleIdOne, collapsibleIdTwo) {
		product = ($scope.products[lineProducts][index])
		if($("#"+lineProducts+"ThirdAcc_"+index.toString()).hasClass( "active-accordian" ))
		{
			$("#"+lineProducts+"ThirdAcc_"+index.toString()).removeClass("active-accordian")
		}
		else{

		$("#"+lineProducts+"ThirdAcc_"+index.toString()).addClass("active-accordian")
		}

		$("#"+lineProducts+"FirstAcc_"+index.toString()).removeClass("active-accordian")
		$("#"+lineProducts+"SecondAcc_"+index.toString()).removeClass("active-accordian")


		collapsibleIdOne = '#' + collapsibleIdOne;
		collapsibleIdTwo = '#' + collapsibleIdTwo
		$(collapsibleIdOne + product["SKU"]).collapse("hide")
		$(collapsibleIdTwo + product["SKU"]).collapse("hide")


	
		var data = google.visualization.arrayToDataTable([
			['Grade', 'Quantity'],
			['Grade A', 11],
			['Grade B', 2],
			['Grade C', 2],
			['Grade D', 2]
		]);

		var options = {
			title: 'Total stock across all stores',
			is3D: true,
			pieHole: 0.4,
			legend: 'none',
			colors: ["#59A14E", "#EDC949", "#E15759", "#9C755F"],
			chartArea: { width: 330, height: 150 },
			backgroundColor: "#7F7F7F",
			titleTextStyle: {
				color: '#fff'
			}
		};

		var chart = new google.visualization.PieChart(document.getElementById(id + product["SKU"]));
		chart.draw(data, options);
	}


	$scope.showFactors = function (index, lineProducts, collapsibleIdOne, collapsibleIdThree) {
		product = ($scope.products[lineProducts][index])
		$("#"+lineProducts+"FirstAcc_"+index.toString()).removeClass("active-accordian")
		$("#"+lineProducts+"ThirdAcc_"+index.toString()).removeClass("active-accordian")
			

		if($("#"+lineProducts+"SecondAcc_"+index.toString() ).hasClass( "active-accordian" ))
		{
			$("#"+lineProducts+"SecondAcc_"+index.toString()).removeClass("active-accordian")
		}
		else{

		$("#"+lineProducts+"SecondAcc_"+index.toString()).addClass("active-accordian")
		}

		
		


		collapsibleIdOne = '#' + collapsibleIdOne
		collapsibleIdThree = '#' + collapsibleIdThree
		$(collapsibleIdOne + product["SKU"]).collapse("hide")
		$(collapsibleIdThree + product["SKU"]).collapse("hide");
		console.log(product);


		
	}


	$scope.drawLineChart = function (index, lineProducts, id, collapsibleIdTwo, collapsibleIdThree , accordID) {
		product = ($scope.products[lineProducts][index])
		
		

		
		


		if($("#"+lineProducts+"FirstAcc_"+index.toString() ).hasClass( "active-accordian" ))
		{
			
			$("#"+lineProducts+"FirstAcc_"+index.toString()).removeClass("active-accordian")
		}
		else{
			
		$("#"+lineProducts+"FirstAcc_"+index.toString()).addClass("active-accordian")
		}


		$("#"+lineProducts+"ThirdAcc_"+index.toString()).removeClass("active-accordian")
		$("#"+lineProducts+"SecondAcc_"+index.toString()).removeClass("active-accordian")

		collapsibleIdTwo = '#' + collapsibleIdTwo;
		collapsibleIdThree = '#' + collapsibleIdThree;
		$(collapsibleIdTwo + product["SKU"]).collapse("hide")
		$(collapsibleIdThree + product["SKU"]).collapse("hide");
		$(collapsibleIdThree + product["SKU"]).collapse("hide");


	

		var data = google.visualization.arrayToDataTable([
			['Day', 'Price', 'Stock'],
			['Day 1', 20000, 15000],
			['Day 2', 28000, 16800],
			['Day 3', 25000, 20800],
			['Day 4', 33000, 11000],
			['Day 5', 20050, 22000],
			['Day 6', 21000, 15000],
			['Day 7', 32000, 16800],
			['Day 8', 30000, 20800],
			['Day 9', 35000, 11000],
			['Day 10', 25000, 22000]
		]);


		var options = {
			width: 800,
			height: 250,
			backgroundColor: "#7F7F7F",
			title: 'Price vs Sales Volume (last 10 days)',
			chartArea: { left: '17%', top: '30%', bottom: '20%', right: '16%' },
			colors: ["#E15759", "#4E79A7"],
			// fontSize: 20,
			// hAxis: { textStyle: { color: '#fff', }, baselineColor: '#fff' },
			series: {
				0: { targetAxisIndex: 0 },
				1: { targetAxisIndex: 1 }
			},
			vAxes: {
				// Adds titles to each axis.
				0: { title: 'Stock' },
				1: { title: 'Price ($)' },

			},
			hAxis: { textStyle: { color: '#fff', }, baselineColor: '#fff' },
			series: {
				0: { targetAxisIndex: 0 },
				1: { targetAxisIndex: 1 }
			},
			vAxis: {
				minValue: 0, title: 'Price & Stock', titleTextStyle: {
					color: '#fff'
				}, baselineColor: '#fff',
				textStyle: { color: '#fff', }
			},
			// vAxis: {
			// 	minValue: 0, textPosition: 'none', title: 'Price & Stock', titleTextStyle: {
			// 		color: '#fff'
			// 	}, baselineColor: '#fff'
			// },
			legend: { position: 'none' },
			titleTextStyle: {
				color: '#fff'
			}
		};

		element = id + product["SKU"];

		// document.getElementById(element).style.backgroundColor = "";

		var chart = new google.visualization.LineChart(document.getElementById(element));
		chart.draw(data, options);
	}


	$scope.alertItem = function (product) {
		if (product['alerted']) { product['alerted'] = false; }
		else { product['alerted'] = true; }
	}








	$scope.drawSmallerSection = function (index, lineProducts, targetLine, targetPie) {

		// changing + button to -
		var idx = 'plus-button-' + $scope.products[lineProducts][index]['SKU'];
		var plusButton = document.getElementById(idx);
		var plusButtonText = document.getElementById(idx).innerText;
		if (plusButtonText === '+') {
			document.getElementById(idx).innerText = '-';
			plusButton.style.paddingLeft = "11px";
		} else {
			document.getElementById(idx).innerText = '+';
			plusButton.style.padding = "26px 8px";
		}


		product = ($scope.products[lineProducts][index])
		var data = google.visualization.arrayToDataTable([
			['Day', 'Price', 'Stock'],
			['1', 20000, 15000],
			['2', 28000, 16800],
			['3', 25000, 20800],
			['4', 33000, 11000],
			['5', 20050, 22000],
			['6', 21000, 15000],
			['7', 32000, 16800],
			['8', 30000, 20800],
			['9', 35000, 11000],
			['10', 25000, 22000]
		]);

		var options = {
			width: '100%',
			height: 250,
			backgroundColor: "#33826d",
			title: 'Price vs Sales Volume (last 10 days)',
			chartArea: { left: '25%', top: '20%', bottom: '20%', right: '24%' },
			// chartArea: { left: '10%', top: '30%', bottom: '40%', right: '14%' },
			colors: ["#E15759", "#4E79A7"],
			// fontSize: 20,
			hAxis: { textStyle: { color: '#fff', }, baselineColor: '#fff' },
			series: {
				0: { targetAxisIndex: 0 },
				1: { targetAxisIndex: 1 }
			},
			vAxes: {
				// Adds titles to each axis.
				0: { title: 'Stock' },
				1: { title: 'Price ($)' }
			},
			//   vAxes: {
			// 	// Adds titles to each axis.
			// 	0: {title: 'Price'},
			// 	1: {title: 'Stock'}
			//   },
			vAxis: {
				minValue: 0,
				// title: 'Price & Stock', 
				titleTextStyle: {
					color: '#fff'
				},
				baselineColor: '#fff',
				textStyle: { color: '#fff', }
			},
			legend: { position: 'none' },
			titleTextStyle: {
				color: '#fff'
			}
		};
		element = targetLine + product["SKU"];
		var chart = new google.visualization.LineChart(document.getElementById(element));
		console.log(document.getElementById(element));
		chart.draw(data, options);

		// PIE
		var data = google.visualization.arrayToDataTable([
			['Grade', 'Quantity'],
			['Grade A', 11],
			['Grade B', 2],
			['Grade C', 2],
			['Grade D', 2]
		]);

		var options = {
			chartArea: { left: 10, top: 0, width: '20%', height: '40%' },
			is3D: true,
			title: 'Total stock across all stores',
			pieHole: 0.4,
			legend: 'none',
			chartArea: { width: '100%', height: '80%' },
			colors: ["#59A14E", "#EDC949", "#E15759", "#9C755F"],
			backgroundColor: "#33826d",
			legend: { position: 'right' },
			titleTextStyle: {
				color: '#fff'
			}
		};

		var chart = new google.visualization.PieChart(document.getElementById(targetPie + product["SKU"]));
		chart.draw(data, options);
	}
});