from flask import Flask, render_template,json ,jsonify, request ,session, redirect ,url_for
from bson import json_util
from functools import wraps
from pymongo import MongoClient
from config import config
Mongoclient = MongoClient(config["ip"])
Mongodb = Mongoclient[config["db"]]
Mongodb.authenticate(config["dbUser"],config["dbPassword"],mechanism='SCRAM-SHA-1')
col=Mongodb[config["collection"]]
app = Flask(__name__,static_url_path='/static')
app.secret_key = 'Blkafhuaohduahdhjl#$@$%^$#^nlfsoindf'

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('rederlogin'))

    return wrap




@app.route('/')
def rederlogin():
    return render_template('index.html',keys=request.args.get('message'))

        

@app.route('/logout')
def rederlogout():
    session.clear()
    return redirect(url_for("rederindex"))

@app.route('/logincheck', methods=['POST'])
def checkLogin():

    data = request.form
    if data['userId'] and data['password']:
        if (data['userId'].lower()==config["user"] and data['password']==config['password']):
            session["logged_in"]=True
            return redirect(url_for("rederindex"))
        else:
            
            return redirect(url_for("rederlogin",message="Username or password mismatch"))
    else:
        return redirect(url_for("rederlogin",message="Please fill up the following fields"))



@app.route('/dash')
@login_required
def rederindex():
	return render_template('dash.html')


@app.route('/getData' ,  methods=['POST'])
@login_required
def getDataFn():
    req = request.get_json(force=True)
    data=col.find({})
    # data=col.find({})
    data=list(data)
    return json_util.dumps({"data":data})




if __name__ == "__main__":
	app.debug=True
	app.run('0.0.0.0',port=5000)